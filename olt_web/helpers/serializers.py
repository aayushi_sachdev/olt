from rest_framework import serializers
from olt_web import models

class StudentSerialzer(serializers.ModelSerializer):

	"""
	To serialize student model into JSON.
	"""

	class Meta:

		model = models.Student
		fields = ('roll_no', 'batch', 'name',)
